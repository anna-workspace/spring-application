package com.example.userdatabase;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserServiceTest1 {

    @Mock
    private UserRepository mockRepo;

    @InjectMocks
    private UserService service;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(service,"userRepository",mockRepo);
    }
/*
    @Test
    public void userService_testUserCreation_successful() {
        this.setup();
        User user1 = service.create("Anna", "anna@gmail.com", "90123456");
        User returned = mockRepo.findByNumber("90123456");
        assertEquals(user1, returned);
    }

    @Test
    public void userService_testGetAll_successful() {
        this.setup();
        User user1 = service.create("Anna", "anna@gmail.com", "90123456");
        User user2 = service.create("Amanda", "amanda@gmail.com", "98765432");
        User user3 = service.create("Liting", "liting@gmail.com", "90001234");
        List<User> list = Arrays.asList(user1, user2, user3);
        assertEquals(list, mockRepo.findAll());
    }

    @Test
    public void userService_testUpdate_successful() {
        this.setup();
        User user1 = service.create("Anna", "anna@gmail.com", "90123456");
        service.update("Anna", "anna@hotmail.com", "91112345");
        assertAll(
                () -> assertEquals("anna@hotmail.com", user1.getEmail()),
                () -> assertEquals("91111234", user1.getNumber())
        );
    }
*/
    @Test
    public void userService_testDelete_successful() {
        User user1 = service.create("Anna", "anna@gmail.com", "90123456");
        service.delete("90123456");
        List<User> list = new ArrayList<>();
        assertEquals(list, service.getAll());
    }

    @Test
    public void userService_testDeleteAll_successful() {
        User user1 = service.create("Anna", "anna@gmail.com", "90123456");
        User user2 = service.create("Amanda", "amanda@gmail.com", "98765432");
        User user3 = service.create("Liting", "liting@gmail.com", "90001234");
        service.deleteAll();
        List<User> list = new ArrayList<>();
        assertEquals(list, mockRepo.findAll()); //should return empty list
    }
}