package com.example.userdatabase.data;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class BloombergDataServiceTests {

    private BloombergDataService bloombergDataService;

    @Before
    public void setUp_validApiKeyHostUrl() {
        bloombergDataService = new BloombergDataService();
        ReflectionTestUtils.setField(bloombergDataService, "HOST", "bloomberg-market-and-financial-news.p.rapidapi.com");
        ReflectionTestUtils.setField(bloombergDataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7e");
    }

    @Before
    public void setUp_invalidApiKey() {
        bloombergDataService = new BloombergDataService();
        ReflectionTestUtils.setField(bloombergDataService, "HOST", "bloomberg-market-and-financial-news.p.rapidapi.com");
        ReflectionTestUtils.setField(bloombergDataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7");
    }


    @Test
    public void getData_validApiKeyHostUrl() {
        setUp_validApiKeyHostUrl();
        JSONObject json = bloombergDataService.getBloombergNewsData();
        assertNotEquals(json, null);
    }

    @Test
    public void getData_invalidApiKey() {
        setUp_invalidApiKey();
        JSONObject actual = bloombergDataService.getBloombergNewsData();
        JSONObject expected = new JSONObject();
        expected.put("message", "You are not subscribed to this API.");
        assertEquals(expected.get("message"), actual.get("message"));
    }


}
