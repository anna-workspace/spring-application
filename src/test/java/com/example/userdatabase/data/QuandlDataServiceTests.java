package com.example.userdatabase.data;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

public class QuandlDataServiceTests {

    private QuandlDataService quandlDataService;

    @Before
    public void setUp_validApiKeyHostUrl() {
        quandlDataService = new QuandlDataService();
        ReflectionTestUtils.setField(quandlDataService, "HOST", "quandl1.p.rapidapi.com");
        ReflectionTestUtils.setField(quandlDataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7e");
    }

    @Before
    public void setUp_invalidApiKey() {
        quandlDataService = new QuandlDataService();
        ReflectionTestUtils.setField(quandlDataService, "HOST", "quandl1.p.rapidapi.com");
        ReflectionTestUtils.setField(quandlDataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7");
    }

    @Test
    public void getUrl_validHostParameters() {
        setUp_validApiKeyHostUrl();
        JSONObject json = quandlDataService.getQuandlMetadata("AMZ");
        assertNotNull(json.toString());
    }

    @Test
    public void getData_invalidAPI() {
        setUp_invalidApiKey();
        JSONObject actual = quandlDataService.getQuandlMetadata("AMZ");
        JSONObject expected = new JSONObject();
        expected.put("message", "You are not subscribed to this API.");
        assertEquals(expected.get("message"), actual.get("message"));
    }

    @Test
    public void getData_invalidTicker() {
        setUp_validApiKeyHostUrl();
        JSONObject json = quandlDataService.getQuandlMetadata("ALASDFFSD");
        JSONObject error = json.getJSONObject("quandl_error");
        String expectedMsg = "You have submitted an incorrect Quandl code. Please check your Quandl codes and try again.";
        assertEquals(expectedMsg, error.get("message"));
    }

}
