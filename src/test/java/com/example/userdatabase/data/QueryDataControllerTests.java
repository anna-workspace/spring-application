package com.example.userdatabase.data;

import com.example.userdatabase.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class QueryDataControllerTests {

    @Mock
    QueryDataService dataService;

    @Mock
    YahooDataService yahooDataService;

    @Mock
    UserService userService;

    @InjectMocks
    QueryDataController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(yahooDataService, "HOST", "apidojo-yahoo-finance-v1.p.rapidapi.com");
        ReflectionTestUtils.setField(yahooDataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7e");
        ReflectionTestUtils.setField(dataService, "yahooDataService", yahooDataService);
        ReflectionTestUtils.setField(controller, "queryDataService", dataService);
        ReflectionTestUtils.setField(controller, "userService", userService);
    }

    @Test
    public void getRequest_invalidApiKey() {
        setUp();
        ResponseEntity re = controller.getWatchList("AAAAAAAAAAAA");
        assertEquals(re.getStatusCode(), HttpStatus.UNAUTHORIZED);
    }

}
